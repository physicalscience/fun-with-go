package exercises

func Fib(i int, e []int) []int {
	if i < len(e) {
		if i == 0 { 
			e[i] = 0
		} else if i == 1 {
			 e[i] = 1 
		} else { e[i] = e[i-1] + e[i-2] }
		i++
		return Fib(i, e)
	}
	return e
}