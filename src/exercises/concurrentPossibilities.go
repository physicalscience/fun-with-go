package exercises

import "fmt"

func GenerateAllPossibilities() {
	set := []int{1,2,3,4,5,6,7,8,9}
	op := []bool{true, true, true, true, true, true, true, true}
	endIndex := 8
	index := 0
	for i:= 0; i < 128 ; i++ {
		go executePossibility(op, set)
		if index == endIndex {
			if(endIndex == 0) {break}
			for i := 0; i < endIndex; i++ {
				op[i] = true
			}
			op[endIndex-1] = false
			endIndex--
			index = 0
		} else {
			op[index] = false
			index++
		}
	}
}

func executePossibility(op[]bool, set[]int) {
	fmt.Println("Executing... {", op[0], ",", op[1], ",", op[2], ",", op[3], ",", op[4], ",", op[5], ",", op[6], ",", op[7], "}")
	runner := 0
	for i := 0; i < len(op) ; i++ {
		if op[i] {
			runner += set[i]
		} else {
			runner -= set[i]
		}
	}
	if runner == 100 {
		fmt.Println("100 Reached!")
	} else {
		fmt.Println("Nope!")
	}
}