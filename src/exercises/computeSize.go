package exercises

func ComputeSizeFor(l[] int) int {
	c := 0
	for i := 0; i < len(l); i++ {
		c += l[i]
	}
	return c
}

func ComputeSizeWhile(l[] int) int {
	c := 0
	i := 0
	for i < len(l) {
		c += l[i]
		i++
	}
	return c
}

func ComputeSizeRec(l[] int, i, c int) int {
	if i < len(l) {
		c += l[i]
		i += 1
		 return ComputeSizeRec(l, i, c)
	}
	return c
}