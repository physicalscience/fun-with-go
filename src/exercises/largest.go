package exercises

import (
	"sort"
	"strconv"
)
/**
 * Sorts a splice and passes it to largeRec
 * @param e - The integer splice to build a number out of
 * @return string - returns a string representing the largest number possible
**/
func LargestNumber(e[] int) string {
	sort.Ints(e)
	return largeRec(e, "")
}

func largeRec(e[]int, s string) string {
	if len(e) != 0 {
		i := len(e)-1
		f := s + strconv.Itoa(e[i])
		e = append(e[:i], e[i+1:]...)
		return largeRec(e, f)
	} else {
		return s
	}
}
