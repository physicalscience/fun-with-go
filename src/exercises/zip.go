package exercises

func Slice(a[] int, b[] int) []int {
	i := 0
	j := 0
	var big []int
	var small []int
	var newArray []int
	op := true
	if(len(a) > len(b)) {
		big = a
		small = b
	} else {
		big = b
		small = a
	}
	newArray = make([]int, len(small), len(small))
	for (i + j) < len(small) {
		if op {
			newArray[i+j] = big[i]
			i++
 		} else {
			 newArray[i+j] = small[j]
			 j++
		 }
		 op = !op
	}
	return newArray
}