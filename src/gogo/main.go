package main

import (
	//"fmt"
	"exercises"
)

func main() {
	// l := []int{1, 2, 3, 4, 5, 6, 7, 8}
	// z := []int{10, 11, 16, 14, 15, 35, 42}
	// m := []int{12, 24, 98, 54, 42}
	// f := make([]int, 20, 20)
	// fmt.Println("Compute the sum of Elements of an array {1, 2, 3, 4}: ")
	// fmt.Println("")
	// fmt.Println(exercises.ComputeSizeFor(l))
	// fmt.Println(exercises.ComputeSizeWhile(l))
	// fmt.Println(exercises.ComputeSizeRec(l, 0, 0))
	// fmt.Println("")
	// fmt.Println("Zipping two arrays together {10, 11, 16, 14, 15}"," and {1, 2, 3, 4} : ")
	// fmt.Println("")
	// q := exercises.Slice(l, z)
	// for i:= 0; i < len(q); i++ {
	// 	fmt.Println(q[i])
	// }
	// fmt.Println("")
	// fmt.Println("Printing Fib numbers up to the first 20")
	// fmt.Println("")
	// e := exercises.Fib(0, f)
	// for i:= 0; i < len(e); i++ {
	// 	fmt.Println(e[i])
	// }
	// fmt.Println("")
	// fmt.Println("turning an array of integers into the largest integer possible ", "{12, 24, 98, 54, 42} : ")
	// fmt.Println("")
	// fmt.Println(exercises.LargestNumber(m))
	exercises.GenerateAllPossibilities()
}